"""
Tool for extracting translatable resources from JavaScript files used in
Pillow and WAF subsystems of Kindle Touch GUI.

Translatable resources of Pillow are located in separate JavaScript files at
directory '/usr/share/webkit-1.0/pillow/strings'.

Translatable resources of WAF app are located in separate JavaScript file at
directory of this app (/var/local/waf/<app_name>/). There are no common scheme
of placement of WAF resources for concrete WAF app. Some examples: for adviewer
app it's 'scripts/resources.js', for browser app it's 'js/strings.js'.

Structure of these JavaScript files is mostly common. There is one variable
assignment with value of object literal. Keys of object are resources names and
values are translatable strings. Some values could be object literals by itself
which keys are resources names and values are either translatable strings or
object literals etc.

Example:

    var AppStringTable = {
        helloMessage:   "Hello",
        byeMessage:     "Bye",
        statusMessages: {
          ok:    "OK",
          error: "Error"
        }
    };

Some JavaScript files contains specific code (though, in the end it is evaluated
to common scheme with one assignment of object literal). And some values of
object literal members are specific too.

Here is the list of exceptions from common scheme:
* /usr/share/webkit-1.0/pillow/strings/media_player_bar_strings.js

  It contains value that is costructor of new object, like:

      var StringTable = { message: new Template("Hello, <% name %>") };

* /var/local/waf/adviewer/scripts/resources.js

  Assigned object literal is wrapped into function call, like:

      var AppResources = (function() { 
        var messages = { hello: "Hello" };
        return {
          messages: messages
        };
      } ());

* /var/local/waf/browser/js/strings.js

  There is no local scope variable assignment (that with 'var' keyword), but
  rather assignment to a new member of previously created object, like:

      wafapp.messages = { ... };

* /var/local/waf/browser/js/strings.js

  One of value contained in assigned object literal is array, like

      wafapp.messages = { 
        locations: [
          {
            name: 'First location',
            path: '/some/path'
          },
          {
            name: 'Second location',
            path: '/some/path/too'
          }
        ]
      };

These exceptions are taken in account in this tool.
"""

import sys
import jsparser

def guess_assignment_name(identifier_node):
    """
    ASSIGN node represents assignment expression. Left part of assignment 
    (identifier of variable) could be:
    * IDENTIFIER node (in case of assignment 'val = 5')
    * DOT node contained IDENTIFIER nodes (in case of assignment 'val.mem = 5')

    Assignment name is a list with names of identifiers.

    This is an ad-hoc hack for parsing browser WAF app resources, where
    top-level assignment of resources variable is represented as
    'wafapp.messages = { ... };'.
    """
    if 'IDENTIFIER' == identifier_node.type:
        return [identifier_node.value]
    elif 'DOT' == identifier_node.type:
        return map(lambda v: v.value, identifier_node)

def guess_string_values_of_property(property_init_node, ns):
    """
    Ad-hoc hacks for parsing of resources of media player and browser.

    In media player resources some value is represented as constructor call.
    In browser app some value is array literal contained object literals.

    Function iterates over found values returning found translatable strings
    in form of tuple (<resource_key>, <resource_string>).
    """
    if 'NEW_WITH_ARGS' == property_init_node.type:
        # Extract string argument of constructor call.
        # For identifying of constructor call add ' (ConstructorName)' to
        # last object key name in hierarchy.
        constructor = property_init_node[0].value
        qualified_name = '.'.join(ns) + ' (%s)'%constructor
        yield (qualified_name, property_init_node[1][0].value)
    elif 'ARRAY_INIT' == property_init_node.type:
        for idx, element_node in enumerate(property_init_node):
            if 'OBJECT_INIT' == element_node.type:
                element_ns = list(ns)
                # For identifying of array literal add ' [<element_idx>]' to
                # last object key name in hierarchy.
                element_ns[-1] += ' [%d]'%idx
                for property_node in element_node:
                    qualified_name = '.'.join(element_ns + [property_node[0].value])
                    yield (qualified_name, property_node[1].value)

def recursive_search_for_string(node, ns=None):
    """
    Recurse into node representing top-level assignment and yield found
    translatable strings in form of tuple (<resource_key>, <resource_string>).

    Resource key is a string with names of top-level assigned variable and 
    assigned object literal keys, delimited by dot.

    For top-level assignment of 'var a = { b: "c", d: "e"}', this functions
    will yield tuples ('a.b', 'c') and ('a.d', 'e').
    """
    if 'PROPERTY_INIT' == node.type:
        name, initializer = [node[0].value], node[1]
    elif 'ASSIGN' == node.type:
        name, initializer = guess_assignment_name(node[0]), node[1]
    else:
        name, initializer = [node.value], node.initializer
    qualified_name = name if ns is None else ns + name
    if 'STRING' == initializer.type:
        name = '.'.join(qualified_name)
        yield (name, initializer.value)
    elif 'OBJECT_INIT' == initializer.type:
        for property_init in initializer:
            for s in recursive_search_for_string(property_init, qualified_name):
                yield s
    elif 'PROPERTY_INIT' == node.type:
        for s in guess_string_values_of_property(initializer, qualified_name):
            yield s

def o(script_node, ns=None):
    """
    Find top-level assignments in parsed JavaScript code, take first of them
    and make a recursive search for translatable string resources.
    """
    if len(script_node.varDecls):
        assignment_node = script_node.varDecls[0]
        value_node = assignment_node.initializer
    else:
        # Ad-hoc hack for browser resources where top-level assignment is made
        # without 'var' keyword.
        assignment_node = script_node[0].expression
        value_node = assignment_node[1]
    if 'GROUP' == value_node.type:
        # Ad-hoc hack for adviewer resources where assigned object literal is
        # wrapped into function call.
        #
        # Recurse into 'SCRIPT' node representing code inside called function.
        o(value_node[0][0].body, [assignment_node.value])
    elif 'OBJECT_INIT' == value_node.type:
        for s in recursive_search_for_string(assignment_node, ns):
            print(s)

if __name__ == "__main__":
    o(jsparser.parse(file(sys.argv[1]).read(),sys.argv[1]))
