What this tool does is:
   * compares "model" language and source language and finds all properties that are identical (if "model" language property is missing or
emtpty it is considered identical to source language)
   * compares one or several other languages with source language and points out properties that are identical in model<->source but not identical in target<->source

For usage tips run "python checkit.py -h"

The tool doesn't mind your working dir, you may run it from whatever you like, just don't move it and don't rename src/ directory.

Typical way to check stuff is to run "python checkit.py -m de" assuming "de" localization is fine and then browsing the reports into report/ directory.

Remember to do fresh "tx pull -a" before running the tool so you'd get fresh results

