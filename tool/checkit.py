import os, glob, collections, sys, re
from optparse import OptionParser

def getFrameworkVersions(path):
    return os.walk(path).next()[1]

def loadProperyFile(path):
    data = []
    with open(path, 'r') as f:
        for line in f.xreadlines():
            pair = line.strip().split('=', 1)
            if len(pair) == 2:
                data.append(pair)
    return dict(data)

def getAllProperties(path):
    prop = re.compile(r'(?P<name>[^_]*?)_(?P<lng>.*)\.properties')
    result = collections.defaultdict(lambda: collections.defaultdict(dict))
    for root, dirs, files in os.walk(path):
        shortName = os.path.relpath(root, path)
        for f in files:
            pp = prop.match(f)
            try:
                name, lng = pp.group('name'), pp.group('lng')
                result[shortName][name][lng] = loadProperyFile(os.path.join(root, f))
            except:
                pass
    return result

def getAllLangs(properties):
    result = set()
    for propFiles in properties.itervalues():
        for lngDict in propFiles.itervalues():
            result |= set(lngDict.keys())
    return result

def getEqualKeys(src, target):
    result = set()
    for k in src:
        v = target.get(k, '')
        if v == '' or v == src[k]:
            result.add(k)
    return result

def main():
    rootPath = os.path.join(os.path.split(__file__)[0], '..')
    srcPath = os.path.join(rootPath, 'src')
    allFrameworkVersions = getFrameworkVersions(srcPath)

    parser = OptionParser()
    parser.add_option('-f', '--framework-version', dest='frameworkVersion', action='store', help='framework version to check', metavar='VERSION', default='5.0.0')
    parser.add_option('-s', '--source-lng', dest='srcLng', action='store', help='source language', metavar='LANG', default='en_US')
    parser.add_option('-m', '--model-lng', dest='modLng', action='store', help='model language (the one which translation is verified to be working)', metavar='LANG', default='de')
    parser.add_option('-t', '--target-lng', dest='target', action='append', help='language to check (you may specify several languages or omit the option to check all of them)', metavar='LANG', default=[])
    parser.add_option('-o', '--output-dir', dest='output', action='store', help='directory where to put reports', metavar='PATH', default='report')
    options, args = parser.parse_args()
    
    if options.frameworkVersion not in allFrameworkVersions:
        parser.error('Wrong framework version <%s>. Available are: %s' % (options.frameworkVersion, ', '.join(allFrameworkVersions)))
    srcPath = os.path.join(srcPath, options.frameworkVersion)

    reportPath = os.path.join(rootPath, options.output, options.frameworkVersion)
    if not os.path.exists(reportPath):
        try:
            os.makedirs(reportPath)
        except Exception, e:
            print 'Cannot create output directory "%s". %s' % (reportPath, str(e))
            return 1
    elif not os.path.isdir(reportPath):
        print 'Specified output "%s" is not a directory. Quitting' % reportPath
        return 1
    
    print 'Reading l10n, please wait...',
    l10n = getAllProperties(srcPath)
    langs = getAllLangs(l10n)
    langsStr = ', '.join(sorted(list(langs)))
    langNotKnown = '%s language <%s> not known. Known languages are: %s'
    print 'done\n'
    
    if options.srcLng not in langs:
        parser.error(langNotKnown % ('Source', options.srcLng, langsStr))
    if options.modLng not in langs:
        parser.error(langNotKnown % ('Model', options.modLng, langsStr))
    
    if options.target:
        for lang in options.target:
            if lang not in langs:
                parser.error(langNotKnown % ('Target', lang, langsStr))
    else:
        options.target = langs - set([options.srcLng, options.modLng])
    
    print ('Parameters:\n    * ' + ': %s\n    * '.join([x.ljust(20) for x in ['framework version', 
                                                                              'source language', 
                                                                              'model language', 
                                                                              'target language' + ('s' if len(options.target) > 1 else ''), 
                                                                              'output directory']]) + ': %s\n') % \
        (options.frameworkVersion, options.srcLng, options.modLng, ', '.join(sorted(options.target)), reportPath)
    
    print 'Generating report...',
    
    shouldBeEqual = {}
    for folder, folderValue in l10n.iteritems():
        if folderValue:
            tmp = {}
            for fileName, fileValue in folderValue.iteritems():
                if fileValue and options.srcLng in fileValue:
                    eq = getEqualKeys(fileValue[options.srcLng], fileValue.get(options.modLng, {}))
                    if eq:
                        tmp[fileName] = eq
            if tmp:
                shouldBeEqual[folder] = tmp
    
    report = {}
    for lang in options.target:
        tmpLang = {}
        for folder, folderValue in shouldBeEqual.iteritems():
            tmpFolder = {}
            for fileName, equalProps in folderValue.iteritems():
                tmpEq = getEqualKeys(l10n[folder][fileName][options.srcLng], l10n[folder][fileName].get(lang, {}))
                bad = equalProps - tmpEq
                if bad:
                    tmpFolder[fileName] = bad
            if tmpFolder:
                tmpLang[folder] = tmpFolder
        if tmpLang:
            report[lang] = tmpLang
    
    print 'done'
    print 'Saving report...',
    
    for lang, data in report.iteritems():
        if not data: 
            continue
        with open(os.path.join(reportPath, '%s.html' % lang), 'w') as out:
            out.write('''<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <table border=1>
            <tr><td>Filename</td><td>Property</td><td>%s value</td><td>%s value</td></tr>''' % (lang, options.srcLng))
            for folder, folderData in data.iteritems():
                if not folderData:
                    continue
                out.write('''
            <tr><td colspan=4 align=center><b>%s</b></td></tr>''' % folder)
                for fileName, badProps in folderData.iteritems():
                    if not badProps:
                        continue
                    firstProp = badProps.pop()
                    out.write('''
            <tr><td rowspan=%d>%s_%s.properties</td><td>%s</td><td>%s</td><td>%s</td></tr>''' % \
                    (len(badProps) + 1, fileName, lang, firstProp,
                     l10n[folder][fileName][lang][firstProp],
                     l10n[folder][fileName][options.srcLng][firstProp]))
                    for item in badProps:
                        out.write('''
            <tr><td>%s</td><td>%s</td><td>%s</td></tr>''' % \
                        (item,
                         l10n[folder][fileName][lang][item],
                         l10n[folder][fileName][options.srcLng][item]))
                    # end of for item in badProps
                # end of for fileName, badProps in folderData.iteritems()
            # end of for folder, folderData in data.iteritems()
            out.write('''
        </table>
    </body>
</html>''')
    # end of for lang, data in report.iteritems()
    print 'done'
    print 'Generating index...',
    with open(os.path.join(reportPath, 'index.html'), 'w') as out:
        out.write('''<html>
    <body>
        <ul>''')
        for lang, data in report.iteritems():
            if not data:
                continue
            out.write('''
            <li><a href="%s">%s</li>''' % ('%s.html' % lang, lang))
        # end of for lang, data in report.iteritems()
        out.write('''
        </ul>
    </body>
</html>''')
        out.close()
    print 'done'
    return 0

if __name__ == '__main__':
    sys.exit(main())