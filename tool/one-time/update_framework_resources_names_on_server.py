import base64, json, re, sys, urllib2
from getpass import getpass
from ConfigParser import ConfigParser
from optparse import OptionParser

parser = OptionParser()
parser.add_option('-c', '--tx-config',
                  metavar='PATH',
                  dest='tx_config',
                  default='.tx/config',
                  help='path to the Transifex command-line client '
                        "configuration file ('.tx/config' by default)")
parser.add_option('-u', '--user',
                  dest='user',
                  help='username of account on Transifex server')
parser.add_option('-p', '--password',
                  dest='password',
                  help='password of account on Transifex server')

(options, args) = parser.parse_args()

config = ConfigParser()
if not len(config.read(options.tx_config)):
    print("Configuration file hadn't found at {0}".format(options.tx_config))
    sys.exit()

if not config.has_section('main') or not config.has_option('main', 'host'):
    print("Configuration file doesn't contains Transifex server address")
    sys.exit()
host = config.get('main', 'host')
if host.endswith('/'):
    host = host[:-1]

if options.user is None:
    options.user = raw_input('Username: ')
if options.password is None:
    options.password = getpass('Password: ')
auth_string = base64.encodestring('{0}:{1}'.format(options.user,
                                                   options.password))[:-1]
auth_header = ('Authorization', 'Basic {0}'.format(auth_string))

framework_rc_sections = \
    filter(lambda s: s.startswith('kindle-touch-framework.'), config.sections())
for section_name in framework_rc_sections:
    if not config.has_option(section_name, 'file_filter'):
        print("file_filter option hadn't found at resource section '{0}'. "
              "Skip it's updating.".format(section_name))
        continue
    file_filter = config.get(section_name, 'file_filter')
    match = re.match('^src/([^/]+)/framework/(.+)_<lang>.properties$',
                     file_filter)
    fw_version = match.group(1)
    qualified_class_name = match.group(2).replace('/', '.')
    rc_name = '[{0}] {1}'.format(fw_version, qualified_class_name)

    project_slug, rc_slug = section_name.split('.')
    api_rc_endpoint = '{0}/api/2/project/{1}/resource/{2}/'.format(host,
                                                                   project_slug,
                                                                   rc_slug)
    try:
        req_get = urllib2.Request(api_rc_endpoint)
        req_get.add_header(*auth_header)
        rc = json.load(urllib2.urlopen(req_get))
        print("Resource {0}".format(rc_slug))
        if rc_name == rc['name']:
            print("  already has human-readable name. Skip it's updating.")
        else:
            print("  hasn't human-readable name. Updating it.")
            opener = urllib2.build_opener(urllib2.HTTPHandler)
            req_put = urllib2.Request(api_rc_endpoint,
                                      data=json.dumps({'name': rc_name}))
            req_put.add_header(*auth_header)
            req_put.add_header('Content-Type', 'application/json')
            req_put.get_method = lambda: 'PUT'
            opener.open(req_put)
    except urllib2.HTTPError as e:
        print("Access to Transifex server has failed: {0}".format(e))

