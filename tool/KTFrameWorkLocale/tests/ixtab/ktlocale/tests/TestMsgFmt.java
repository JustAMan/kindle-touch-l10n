package ixtab.ktlocale.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import ixtab.ktlocale.MsgFmt;
import ixtab.ktlocale.ShellCommand;
import ixtab.ktlocale.ShellCommand.Result;
import ixtab.ktlocale.tests.resources.TestResources;

import org.junit.Test;


public class TestMsgFmt {
	@Test
	public void testConstructors() throws Exception {
		try {
			new MsgFmt("illegalcommand");
			fail();
		} catch (IllegalArgumentException e) {};
		new MsgFmt(null);
	}
	
	@Test
	public void testInputOutput() throws Exception {
		File src = File.createTempFile("msgfmt-in-", ".po");
		src.deleteOnExit();
		File trg = File.createTempFile("msgfmt-out-", ".mo");
		trg.deleteOnExit();
		
		// quick-n-dirty copy
		InputStream is = TestResources.class.getResourceAsStream("wua.po");
		assertNotNull(is);
		FileOutputStream os = new FileOutputStream(src);
		for (int i=is.read(); i != -1; i=is.read()) {
			os.write(i);
		}
		os.close();
		is.close();
		
		new MsgFmt(null).format(src,trg);
		Result r = ShellCommand.exec("md5sum "+trg.getCanonicalPath());
		assertTrue(r.out.contains("890b788737b229320756bb1d8335b7bd"));
	}
}
