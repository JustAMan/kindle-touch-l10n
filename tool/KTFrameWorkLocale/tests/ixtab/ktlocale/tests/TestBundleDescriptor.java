package ixtab.ktlocale.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import ixtab.ktlocale.BundleDescriptor;
import ixtab.ktlocale.tests.resources.TestResources;

import org.junit.Test;


public class TestBundleDescriptor {
	public static final File ROOT = TestResources.getAsFile();
	
	@Test
	public void testFinding() throws Exception {
		List<BundleDescriptor> list = BundleDescriptor.find(ROOT);
		assertTrue(list.isEmpty());
		list = BundleDescriptor.find(ROOT, "de_DE","fr");
		assertEquals(3, list.size());
		for (BundleDescriptor d: list) {
			assertTrue(d.packageName.equals("com.amazing.properties.sample"));
		}
	}
}
