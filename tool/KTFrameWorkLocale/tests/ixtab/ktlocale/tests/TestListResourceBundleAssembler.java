package ixtab.ktlocale.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import ixtab.ktlocale.KTResourceBundle;
import ixtab.ktlocale.ListResourceBundleAssembler;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.Locale;
import java.util.zip.GZIPOutputStream;

import org.junit.Test;

import serp.bytecode.BCClass;
import serp.bytecode.lowlevel.Entry;
import serp.bytecode.lowlevel.UTF8Entry;

import com.amazon.ebook.util.text.Base64;

public class TestListResourceBundleAssembler {
	
	private static final Object[][] CONTENTS = new Object[][] {
		new Object[] {
				"simple", "entry"
		}
	};

	@Test
	public void testBase64() throws Exception {
		String pkg = "test.ixtab";
		String cls = "TestProperties";
		final Locale locale = Locale.CANADA_FRENCH;
		
		BCClass verify = ListResourceBundleAssembler.assembleFrom(new KTResourceBundle(CONTENTS, pkg,cls,locale));		
		assertEquals(pkg, verify.getPackageName());
		assertEquals(cls+"_"+locale, verify.getClassName());
		
		// this is a lame way of checking.
		for (Entry entry: verify.getPool().getEntries()) {
			if (entry instanceof UTF8Entry) {
				String value = ((UTF8Entry) entry).getValue();
				if (ListResourceBundleAssembler.DEFAULT_CONTENTS.equals(value)) {
					fail("default contents should have been replaced.");
				}
			}
		}
		String expectedResult = stringifyUsingAmazonBase64();
		
		boolean found = false;
		for (Entry entry: verify.getPool().getEntries()) {
			if (entry instanceof UTF8Entry) {
				String value = ((UTF8Entry) entry).getValue();
				if (expectedResult.equals(value)) {
					found = true;
					break;
				}
			}
		}
		if (!found) {
			fail("expected result not found");
		}
	}

	private String stringifyUsingAmazonBase64() throws Exception {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		GZIPOutputStream gos = new GZIPOutputStream(bos);
		ObjectOutputStream oos = new ObjectOutputStream(gos);
		oos.writeObject(CONTENTS);
		oos.close();
		String result = Base64.encodeBytes(bos.toByteArray());
		result = result.replaceAll("[\\r\\n]", "");
		return result;
	}
	
	@Test
	public void test() throws Exception {
		
	}
}
