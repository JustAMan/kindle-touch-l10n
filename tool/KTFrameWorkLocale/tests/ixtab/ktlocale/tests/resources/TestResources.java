package ixtab.ktlocale.tests.resources;

import java.io.File;
import java.net.URISyntaxException;

public class TestResources {
	public static File getAsFile() {
		try {
			return new File(TestResources.class.getResource("").toURI());
		} catch (URISyntaxException e) {
			// won't happen.
			e.printStackTrace();
			return null;
		}
	}
}
