package ixtab.ktlocale.tests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import ixtab.ktlocale.KTResourceBundle;
import ixtab.ktlocale.BundleDescriptor;
import ixtab.ktlocale.tests.resources.TestResources;

import java.util.Enumeration;
import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import org.junit.Ignore;
import org.junit.Test;


public class TestKTResourceBundle {
	
	@Test
	public void testEntriesAreCompleteAndSorted() throws Exception {
		ResourceBundle original = new ListResourceBundle() {
			
			@Override
			protected Object[][] getContents() {
				return new Object[][] {
						new Object[] {
								"one", "один",
						},
						new Object[] {
								"two", "два",
						},
						new Object[] {
								"three", "тpи",
						},
						new Object[] {
								"four", "четыpе",
						}
				};
			}
		};
		KTResourceBundle in = new KTResourceBundle(original);
		Object[][] contents = in.getContents();
		String last = "";
		int i = 0;
		for (Object[] values : contents) {
			String key = (String) values[0];
			assertTrue("!!! " + last + " < " + key, last.compareTo(key) < 0);
			last = key;
			++i;
		}
		assertEquals(4, i);
	}
	
	/* This has become a tautology, because locale parsing now takes place
	 * outside of the constructor. Locale parsing is also checked in the
	 * next test.
	 */
	@Test
	@Ignore
	public void testLocaleParsing() throws Exception {
		KTResourceBundle bundle = new KTResourceBundle(new ResourceBundle() {

			@Override
			protected Object handleGetObject(String key) {
				return "dummy";
			}

			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public Enumeration<String> getKeys() {
				Enumeration x = new StringTokenizer("dummy");
				return x;
			}}, "pkg","cls", Locale.GERMANY);
		assertEquals(Locale.GERMANY, bundle.getLocale());
	}
	
	@Test
	public void testUsingFrenchDummyLocale() throws Exception {
		BundleDescriptor descriptor = BundleDescriptor.find(TestResources.getAsFile(), "fr").get(0);
		KTResourceBundle bundle = KTResourceBundle.fromDescriptor(descriptor);
		assertEquals(Locale.FRENCH, bundle.getLocale());
		
		// sorted
		Object[][] contents = new Object[][] {
				new Object[] {"four","quatre"},	
				new Object[] {"one","un"},	
				new Object[] {"three","trois"},	
				new Object[] {"two","deux"},	
		};
		assertArrayEquals(contents, bundle.getContents());
	}
	
	@Test
	public void testArrayProperties() throws Exception {
		String[] single = new String[] {"single,first","single,second"};
		String[][] _double = new String[][] {
				new String[] {"double,first,first","double,first,second"},	
				new String[] {"double,second,first","double,second,second","double,second,third"},	
		};
		String[][][] triple = new String[][][] {
				new String[][] {
						new String[] {"triple,first,first,first", "triple,first,first,second"},
				},
		};
		
		Object[][] contents = new Object[][] {
				new Object[] {"ixtab.test.array", single},
				new Object[] {"ixtab.test.threearray", triple},
				new Object[] {"ixtab.test.twoarray", _double},
		};
		
		for (BundleDescriptor descriptor: BundleDescriptor.find(TestResources.getAsFile(), "de_DE")) {
			if (descriptor.bundleName.equals("ArrayResourceBundle")) {
				KTResourceBundle bundle = KTResourceBundle.fromDescriptor(descriptor);
				assertArrayEquals(contents, bundle.getContents());
			}
		}
	}
}
