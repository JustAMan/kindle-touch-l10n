package ixtab.ktlocale.tests;

import static org.junit.Assert.assertEquals;
import ixtab.ktlocale.cldr.CLDRLocale;

import java.io.FileOutputStream;
import java.util.Locale;
import java.util.Properties;

import org.junit.Test;


public class TestCLDRLocale {
	
	Locale be = new Locale("be");
	Locale be_BY = new Locale("be","BY");
	Locale de = Locale.GERMAN;
	Locale de_CH = new Locale("de","CH");
	
	@Test
	public void testCompletionAndOrderingWork() throws Exception {
		CLDRLocale[] both = CLDRLocale.load(Locale.GERMAN);
		assertEquals(Locale.GERMAN, both[CLDRLocale.LANGUAGE_ONLY].locale);
		assertEquals(Locale.GERMANY, both[CLDRLocale.LANGUAGE_AND_TERRITORY].locale);
		
		both = CLDRLocale.load(Locale.GERMANY);
		assertEquals(Locale.GERMAN, both[CLDRLocale.LANGUAGE_ONLY].locale);
		assertEquals(Locale.GERMANY, both[CLDRLocale.LANGUAGE_AND_TERRITORY].locale);
	}
	
	@Test
	public void testDescriptions() throws Exception {
		CLDRLocale[] swiss = CLDRLocale.load(de_CH);
		
		assertEquals("Belarusian", CLDRLocale.describe(be, CLDRLocale.ENGLISH));
		assertEquals("Belarusian (Belarus)", CLDRLocale.describe(be_BY, CLDRLocale.ENGLISH));
		
		assertEquals("Weißrussisch", CLDRLocale.describe(be, swiss[0]));
		assertEquals("Weißrussisch (Belarus)", CLDRLocale.describe(be_BY, swiss[0]));

		assertEquals("Weissrussisch", CLDRLocale.describe(be, swiss));
		assertEquals("Weissrussisch (Weissrussland)", CLDRLocale.describe(be_BY, swiss));
	}
	
	@Test
	public void testSelfDescription() throws Exception {
		/* This is not really a test, but more a verification. It includes all locales
		 * that we currently have a translation team for on Transifex.
		 */
		System.out.println(CLDRLocale.getProperties(new Locale("ru","RU")));
		System.out.println(CLDRLocale.getProperties(new Locale("cs")));
		System.out.println(CLDRLocale.getProperties(new Locale("hu","")));
		System.out.println(CLDRLocale.getProperties(new Locale("de","")));
		System.out.println(CLDRLocale.getProperties(new Locale("eu","")));
		System.out.println(CLDRLocale.getProperties(new Locale("hr","HR")));
		System.out.println(CLDRLocale.getProperties(new Locale("it","")));
		System.out.println(CLDRLocale.getProperties(new Locale("es","")));
		System.out.println(CLDRLocale.getProperties(new Locale("uk","UA")));
	}
	
	@Test
	public void testOutput() throws Exception {
		Properties props = CLDRLocale.getProperties(new Locale("ru","RU"));
		props.store(new FileOutputStream("/tmp/ru_RU.properties"), null);
	}
}
