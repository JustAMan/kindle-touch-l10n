package ixtab.ktlocale;

import java.io.File;

import ixtab.ktlocale.ShellCommand.Result;

public class MsgFmt {

	public static final String DEFAULT_EXECUTABLE = "msgfmt";
	private final String executable;
	
	public MsgFmt(String exec) {
		this.executable = exec == null ? DEFAULT_EXECUTABLE : exec;
		try {
			Result r = ShellCommand.exec(this.executable);
			if (r.code == 0) {
				throw new Exception();
			}
		} catch (Exception e) {
			throw new IllegalArgumentException("\""+this.executable+"\" is not a valid executable. Please specify the correct executable using the appopriate option.");
		}
	}

	public void format(File source, File target) {
		String cmd = null;
		try {
			cmd = executable+" -o "+target.getCanonicalPath()+" "+source.getCanonicalPath();
			Result r = ShellCommand.exec(cmd);
			if (r.code != 0) {
				System.err.println("Something went wrong while executing: "+cmd);
				System.err.println(r.err);
			}
		} catch (Exception e) {
			if (cmd != null) {
				System.err.println("Error while running: "+cmd);
			}
			e.printStackTrace();
		}
	}
}
