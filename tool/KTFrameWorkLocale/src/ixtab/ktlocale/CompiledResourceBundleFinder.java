package ixtab.ktlocale;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.List;
import java.util.ResourceBundle;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import contrib.IterableEnumeration;

public class CompiledResourceBundleFinder implements ResourceBundleFinder {

	private static final String FILTER_COM_AMAZON = "com/amazon/";

	@Override
	public void findBundlesInJars(List<ResourceBundle> output,
			List<JarFile> jars) {
		ClassLoader loader = createClassLoaderFor(jars);
		for (JarFile jar: jars) {
			findBundlesInJar(output, jar, loader);
		}
	}

	private ClassLoader createClassLoaderFor(List<JarFile> jars) {
		try {
			URL[] urls = getAsUrlArray(jars);
			return new URLClassLoader(urls, this.getClass().getClassLoader());
		} catch (MalformedURLException ex) {
			// won't happen.
			return null;
		}
	}

	private URL[] getAsUrlArray(List<JarFile> jars) throws MalformedURLException {
		URL[] urls = new URL[jars.size()];
		for (int i=0; i < urls.length; ++i) {
			urls[i] = new File(jars.get(i).getName()).toURI().toURL();
		}
		return urls;
	}

	private void findBundlesInJar(List<ResourceBundle> output, JarFile jar,
			ClassLoader loader) {
		for (JarEntry entry: IterableEnumeration.make(jar.entries())) {
			if (isClassFile(entry)) {
				Class<?> clazz = loadClass(loader, entry);
				if (clazz != null && ResourceBundle.class.isAssignableFrom(clazz)) {
					ResourceBundle instance = instantiate(clazz);
					if (instance != null) {
						output.add(instance);
					}
				}
			}
		}
	}

	private boolean isClassFile(JarEntry entry) {
		String name = entry.getName();
		return (name.endsWith(".class") && !name.contains("$") && name.contains(FILTER_COM_AMAZON));
	}

	private Class<?> loadClass(ClassLoader loader, JarEntry entry) {
		String name = entry.getName();
		name = name.replace(".class", "").replace('/', '.');
		if (name.startsWith(".")) name = name.substring(1);
		try {
			return loader.loadClass(name);
		} catch (Throwable e) {
			//System.err.println("Error loading class: "+ name);
			return null;
		}
	}

	private ResourceBundle instantiate(Class<?> clazz) {
		try {
			ResourceBundle bundle = (ResourceBundle) clazz.newInstance();
			return bundle;
		} catch (Throwable t) {
			System.err.println("Error instantiating: "+clazz.getName());
			t.printStackTrace();
			return null;
		}
	}

}
