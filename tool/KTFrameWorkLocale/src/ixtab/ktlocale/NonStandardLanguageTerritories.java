package ixtab.ktlocale;

import java.util.HashMap;
import java.util.Map;

public class NonStandardLanguageTerritories {
	public static final Map<String, String> MAP = initMappings();
	private static Map<String, String> initMappings() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("cs", "CZ");
		map.put("eu", "ES");
		map.put("et", "EE");
		return map;
	}
}
