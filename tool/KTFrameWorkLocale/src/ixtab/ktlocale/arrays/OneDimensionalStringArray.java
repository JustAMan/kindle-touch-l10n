package ixtab.ktlocale.arrays;

public class OneDimensionalStringArray extends NDimensionalStringArray {
	
	private String[] object = new String[0];
	
	@Override
	public Object getObject() {
		return object;
	}

	@Override
	public void setEntry(String location, String value) {
		ParseResult r = parseLocation(location);
		if (!("".equals(r.rest))) {
			throw new IllegalArgumentException("Illegal array index specification! (found junk past expected end)");
		}
		ensureArrayIsBigEnough(r.index);
		object[r.index] = value;
	}

	private void ensureArrayIsBigEnough(int index) {
		if (object.length <= index) {
			String[] copy = new String[index+1];
			System.arraycopy(object, 0, copy, 0, object.length);
			object = copy;
		}
	}
}
