package ixtab.ktlocale.arrays;

public abstract class NDimensionalStringArray {

	public static int MAX_SUPPORTED_DIMENSIONS = 3;
	
	public static NDimensionalStringArray instantiate(int dimension) {
		switch (dimension) {
		case 1:
			return new OneDimensionalStringArray();
		case 2:
			return new TwoDimensionalStringArray();
		case 3:
			return new ThreeDimensionalStringArray();
		}
		throw new IllegalArgumentException("unsupported dimensionality: "+dimension);
	}

	public abstract Object getObject();

	public abstract void setEntry(String location, String value);
	
	protected static class ParseResult {
		public int index;
		public String rest;

		public ParseResult(int index, String rest) {
			super();
			this.index = index;
			this.rest = rest;
		}
	}
	
	protected ParseResult parseLocation(String location) {
		int bracket = location.indexOf(']');
		if (!location.startsWith("[") || bracket < 0) {
			throw new IllegalArgumentException("Wrong location format: "+location);
		}
		int index = Integer.parseInt(location.substring(1, bracket));
		String rest = location.substring(1, bracket);
		rest = location.substring(bracket+1);
		
		return new ParseResult(index, rest);
	}
}
