package ixtab.ktlocale.arrays;

public class ThreeDimensionalStringArray extends NDimensionalStringArray {
	
	private String[][][] object = new String[0][][];
	private TwoDimensionalStringArray[] delegates = new TwoDimensionalStringArray[0];
	
	@Override
	public Object getObject() {
		return object;
	}

	@Override
	public void setEntry(String location, String value) {
		ParseResult r = parseLocation(location);
		ensureArraysAreBigEnough(r.index);
		if (delegates[r.index] == null) {
			delegates[r.index] = new TwoDimensionalStringArray();
		}
		delegates[r.index].setEntry(r.rest, value);
		object[r.index] = (String[][]) delegates[r.index].getObject();
	}

	private void ensureArraysAreBigEnough(int index) {
		if (object.length <= index) {
			String[][][] copy = new String[index+1][][];
			System.arraycopy(object, 0, copy, 0, object.length);
			object = copy;
			TwoDimensionalStringArray[] nd = new TwoDimensionalStringArray[index+1];
			System.arraycopy(delegates, 0, nd, 0, delegates.length);
			delegates = nd;
		}
	}
}
