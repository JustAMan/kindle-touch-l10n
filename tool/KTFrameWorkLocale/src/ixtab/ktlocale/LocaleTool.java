package ixtab.ktlocale;

import ixtab.ktlocale.kindletool.KindleTool;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;
import com.martiansoftware.jsap.UnflaggedOption;

public class LocaleTool {

	private static final String PARAM_LOCALE = "locale";
	private static final String PARAM_TARGET = "target";
	private static final String PARAM_SOURCE = "source";
	private static final String PARAM_FORCE = "force";
	private static final String PARAM_MODE = "mode";
	private static final String PARAM_MSGFMT = "msgfmt";
	private static final String PARAM_KINDLETOOL = "kindletool";

	private static final String MODE_COMPILE = "compile";
	private static final String MODE_EXTRACT = "extract";
	private static final String MODE_CONVERT = "iso2utf";
	private static final String MODE_DIST = "dist";

	@SuppressWarnings("unused")
	private static String[] sampleExtractArgs = new String[] {
			MODE_EXTRACT,
			"-f",
			"-s",
			System.getProperty("user.home")
					+ "/kindle-touch/fs/opt/amazon/ebook/", "-t",
			"/tmp/kt-loc", };

	@SuppressWarnings("unused")
	private static String[] sampleCompileArgs = new String[] { MODE_COMPILE,
			"-f", "-s", "/tmp/kt-loc", "-t", "/tmp/kt-locale-de.jar", "de", };

	@SuppressWarnings("unused")
	private static String[] sampleDistArgs = new String[] {
			MODE_DIST,
			"-f",
			"-s",
			System.getProperty("user.home")
					+ "/kindle-touch/localization/kindle-touch-l10n-tx-full/src/5.0.0",
			"-t",
			"/tmp/update_locale-LOCALE_TYPE.bin", "ALL", };

	public static void main(String[] args) throws JSAPException {
		// only enable for quick stdargs testing. DO NOT enable or depend on
		// them when checking in.
		// args =sampleCompileArgs;
		// args =sampleExtractArgs;
		// args = sampleDistArgs;

		JSAP jsap = initializeArgumentParser();
		JSAPResult config = validateArguments(args, jsap);
		String source = config.getString(PARAM_SOURCE);
		String target = config.getString(PARAM_TARGET);
		String msgfmt = config.getString(PARAM_MSGFMT);
		String kindletool = config.getString(PARAM_KINDLETOOL);
		boolean force = config.getBoolean(PARAM_FORCE);
		String[] locales = config.getStringArray(PARAM_LOCALE);

		String mode = config.getString(PARAM_MODE);
		if (MODE_EXTRACT.equals(mode)) {
			new ExtractTask(source, target, force).run();
		} else if (MODE_COMPILE.equals(mode)) {
			new CompressTask(source, target, force, locales).run();
		} else if (MODE_CONVERT.equals(mode)) {
			new ConvertTask(source, target, force).run();
		} else if (MODE_DIST.equals(mode)) {
			new DistributionTask(source, target, msgfmt, kindletool, force,
					locales).run();
		} else {
			throw new IllegalArgumentException("Unknown mode: " + mode);
		}
	}

	private static JSAPResult validateArguments(String[] args, JSAP jsap) {
		JSAPResult config = jsap.parse(args);
		if (!config.success()) {
			displayUsage(jsap, config);
		}
		return config;
	}

	private static void displayUsage(JSAP jsap, JSAPResult config) {
		for (@SuppressWarnings("rawtypes")
		java.util.Iterator errs = config.getErrorMessageIterator(); errs
				.hasNext();) {
			System.err.println("Error: " + errs.next());
		}

		System.err.println();
		System.err.println("Usage: java -jar kt-l10n.jar " + jsap.getUsage());
		System.err.println();
		System.err.println(jsap.getHelp());
		System.exit(1);
	}

	private static JSAP initializeArgumentParser() throws JSAPException {
		JSAP jsap = new JSAP();

		UnflaggedOption mode = new UnflaggedOption(PARAM_MODE).setStringParser(
				JSAP.STRING_PARSER).setRequired(true);
		mode.setHelp("One of \""
				+ MODE_EXTRACT
				+ "\", \""
				+ MODE_COMPILE
				+ "\", \""
				+ MODE_DIST
				+ "\", or \""
				+ MODE_CONVERT
				+ "\".\n * "
				+ MODE_EXTRACT
				+ ": finds jars in source directory and extracts relevant resource bundles into target directory.\n * "
				+ MODE_COMPILE
				+ ": compiles all relevant resource files in source into a single file.\n * "
				+ MODE_CONVERT
				+ ": (OBSOLETE) converts all existing .properties files from ISO-8859-1 to UTF-8 format.\n * "
				+ MODE_DIST
				+ ": creates binary update files, combining resources into a format suitable for installation on Kindles.\n"

				+ "");
		jsap.registerParameter(mode);

		Switch force = new Switch(PARAM_FORCE).setShortFlag('f').setLongFlag(
				PARAM_FORCE);
		force.setHelp("Forces execution of operations which would otherwise be deemed unsafe, like directory creation/deletion or overwriting files. Use at own risk.");
		jsap.registerParameter(force);

		FlaggedOption source = new FlaggedOption(PARAM_SOURCE)
				.setShortFlag('s').setLongFlag(PARAM_SOURCE)
				.setStringParser(JSAP.STRING_PARSER).setRequired(true);
		source.setHelp("Source directory to look for input files.\nIn extract mode, directory to search for jar files.\nIn compile mode, directory to search for properties files relevant to the given locales.\nIn dist mode, directory containing translations.");
		jsap.registerParameter(source);

		FlaggedOption target = new FlaggedOption(PARAM_TARGET)
				.setShortFlag('t').setLongFlag(PARAM_TARGET)
				.setStringParser(JSAP.STRING_PARSER).setRequired(true);
		target.setHelp("Target file or directory.\nIn extract mode, directory to write properties files to.\nIn compile mode, jar file to compile resources into.\nIn dist mode, binary file (template) name.");
		jsap.registerParameter(target);

		FlaggedOption msgfmt = new FlaggedOption(PARAM_MSGFMT)
				.setShortFlag('m').setLongFlag(PARAM_MSGFMT)
				.setStringParser(JSAP.STRING_PARSER).setRequired(false);
		msgfmt.setHelp("msgfmt executable to use. Relevant for dist mode only. If not set, \""
				+ MsgFmt.DEFAULT_EXECUTABLE + "\" is used. ");
		jsap.registerParameter(msgfmt);

		FlaggedOption kindletool = new FlaggedOption(PARAM_KINDLETOOL)
				.setShortFlag('k').setLongFlag(PARAM_KINDLETOOL)
				.setStringParser(JSAP.STRING_PARSER).setRequired(false);
		kindletool
				.setHelp("kindletool executable to use. Relevant for dist mode only. If not set, \""
						+ KindleTool.DEFAULT_EXECUTABLE + "\" is used. ");
		jsap.registerParameter(kindletool);

		UnflaggedOption locale = new UnflaggedOption(PARAM_LOCALE)
				.setStringParser(JSAP.STRING_PARSER).setRequired(false)
				.setGreedy(true);
		locale.setHelp("Locales to consider - relevant in compile or dist mode only.\nIn compile mode, not providing a locale results in an output file containing no localizations.\n In dist mode, at least one locale is required. \"ALL\" will create distribution archives for all currently registered locales.");
		jsap.registerParameter(locale);

		return jsap;
	}

}
