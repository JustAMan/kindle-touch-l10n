package ixtab.ktlocale;

import java.io.File;

public class FileDirectoryUtil {
	public static void deleteAllContentRecursively(File directory) {
		if (!directory.isDirectory()) {
			throw new IllegalArgumentException(directory+" must be a directory");
		}
		for (File f: directory.listFiles()) {
			if (f.isDirectory()) {
				deleteAllContentRecursively(f);
			} 
			if (!f.delete()) {
				throw new IllegalStateException(f + " could not be deleted");
			}
		}
	}
}
