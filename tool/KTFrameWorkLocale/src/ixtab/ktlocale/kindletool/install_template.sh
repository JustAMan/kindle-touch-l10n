#!/bin/sh

lipc-send-event com.lab126.blanket.ota otaSplashProgress -i 0
mntroot rw || exit 1

lipc-send-event com.lab126.blanket.ota otaSplashProgress -i 25
test -d /usr/lib/locale/en_US.utf8 || exit 1

if [ `localedef --list-archive|grep -c <LOC_SYSTEM>.utf8` != 1 ]; then
  ln -s /usr/lib/locale/en_US.utf8 /usr/lib/locale/<LOC_SYSTEM>.utf8
fi
lipc-send-event com.lab126.blanket.ota otaSplashProgress -i 50

DIR=`pwd`
cd /
tar xzvf $DIR/locale.tgz
lipc-send-event com.lab126.blanket.ota otaSplashProgress -i 100