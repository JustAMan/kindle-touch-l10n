package ixtab.ktlocale;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.ListResourceBundle;

import com.amazon.ebook.util.text.Base64;

/**
 * This class (rather: its compiled form) is used as a template
 * for producing localized resource bundles.
 * 
 * In other words, all localized resource bundle classes will have
 * the same structure as this class (differing only in package and
 * class name, and of course the "contents").
 * 
 * Modify this class only if you are sure that you know what you're doing.
 * @author ixtab
 *
 */
public class ListResourceBundleTemplate extends ListResourceBundle {

	private static final Object[][] CONTENTS = decodeContents();
	
	@Override
	protected Object[][] getContents() {
		return CONTENTS;
	}

	private static Object[][] decodeContents()  {
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(Base64.decode("H4sIAAAAAAAAAFvzloG1tIhBJDraJyuxLFEvJzEvXc8/KSs1ucRaYv/v4CfZt08xMTBUFDAwMDACFQpjUTfhXMR8gWLNHJg6phIG1rT8opLKEgbmkvJ8ANdAvlljAAAA"));
			//Amazon Base64 *already includes* GZip.
			//GZIPInputStream zis = new GZIPInputStream(bis);
			ObjectInputStream ois = new ObjectInputStream(bis);
			Object[][] result = (Object[][]) ois.readObject();
			ois.close();
			return result;
		} catch (Throwable t) {
			System.err.println("Something which should never have happened actually did happen.");
			return new Object[][] {new Object[] {}};
		}
	}

}
