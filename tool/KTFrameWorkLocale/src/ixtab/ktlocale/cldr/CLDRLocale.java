package ixtab.ktlocale.cldr;

import ixtab.ktlocale.NonStandardLanguageTerritories;

import java.text.MessageFormat;
import java.util.Comparator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

import contrib.SortedProperties;


public class CLDRLocale {
	public static final int LANGUAGE_ONLY = 0;
	public static final int LANGUAGE_AND_TERRITORY = 1;
	public static final CLDRLocale ENGLISH;
	
	public static final String KEY_LOCALE = "locale";
	public static final String KEY_DISPLAY_NAME = "display.name";
	public static final String KEY_LOCALES_SUPPORTED = "locales.supported";
	public static final String KEY_DISPLAY_NAME_LOC_PREFIX = "display.name.";
	public static final String KEY_POSIX_ID = "posix.id";
	

	
	private static final DocumentBuilder BUILDER;
	private static final XPath XPATH;
	private static final XPathExpression XPATH_IDENTITY_LANGUAGE;
	private static final XPathExpression XPATH_IDENTITY_TERRITORY;
	private static final MessageFormat XPATH_LOOKUP_LANGUAGE = new MessageFormat("/ldml/localeDisplayNames/languages/language[@type=\"{0}\"]/text()", Locale.US);
	private static final MessageFormat XPATH_LOOKUP_TERRITORY = new MessageFormat("/ldml/localeDisplayNames/territories/territory[@type=\"{0}\"]/text()", Locale.US);
	private static final Map<String, String> DEFAULT_TERRITORIES = getDefaultTerritories(); 

	
	public final Locale locale;
	private final Document xml;
	
	
	static {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		docFactory.setNamespaceAware(true);
		BUILDER = createBuilder(docFactory);
		XPathFactory factory = XPathFactory.newInstance();
		XPATH = factory.newXPath();
		XPATH_IDENTITY_LANGUAGE = compile("/ldml/identity/language/@type");
		XPATH_IDENTITY_TERRITORY = compile("/ldml/identity/territory/@type");
		ENGLISH = new CLDRLocale(Locale.ENGLISH);
	}

	private static DocumentBuilder createBuilder(DocumentBuilderFactory docFactory) {
		try {
			return docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			return null;
		}
	}
	
	private static XPathExpression compile(String expression) {
		try {
			return XPATH.compile(expression);
		} catch (XPathExpressionException e) {
			return null;
		}
	}

	private static Map<String, String> getDefaultTerritories() {
		return NonStandardLanguageTerritories.MAP;
	}

	public static CLDRLocale[] load(Locale locale) {
		Locale languageOnly, languageAndTerritory;
		if (locale.getCountry().equals("")) {
			languageOnly = locale;
			languageAndTerritory = addTerritory(languageOnly);
		} else {
			languageAndTerritory = locale;
			languageOnly = new Locale(languageAndTerritory.getLanguage());
		}
		
		try {
			return new CLDRLocale[] {new CLDRLocale(languageOnly), new CLDRLocale(languageAndTerritory)};
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	private static Locale addTerritory(Locale languageOnly) {
		String territory = DEFAULT_TERRITORIES.get(languageOnly.getLanguage());
		if (territory == null) {
			territory = languageOnly.getLanguage().toUpperCase(Locale.ENGLISH);
		}
		return new Locale(languageOnly.getLanguage(), territory);
	}
	
	private CLDRLocale(Locale locale) {
		try {
			this.locale = locale;
			this.xml = BUILDER.parse(CLDRLoader.getCLDRFileAsStream(locale.toString()));
			sanityCheck(locale.getLanguage(), XPATH_IDENTITY_LANGUAGE);
			sanityCheck(locale.getCountry(), XPATH_IDENTITY_TERRITORY);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
	}

	private void sanityCheck(String expected,
			XPathExpression expression) {
		if (expected.equals("")) {
			return;
		}
		try {
			Object actual = expression.evaluate(xml, XPathConstants.STRING);
			if (!expected.equals(actual)) {
				throw new IllegalStateException("Expected "+expected+", but got "+actual);
			}
		} catch (XPathExpressionException e) {
		}
	}

	private String[] getDescriptions(Locale lookup) {
		String[] result = new String[2];
		result[0] = evaluate(XPATH_LOOKUP_LANGUAGE, lookup.getLanguage());
		if (!lookup.getCountry().equals("")) {
			result[1] = evaluate(XPATH_LOOKUP_TERRITORY, lookup.getCountry());
		}
		return result;
	}

	private String evaluate(MessageFormat format,
			String content) {
		String expr = format.format(new Object[] {content});
		try {
			String result = XPATH.compile(expr).evaluate(xml);
			if (result != null && result.trim().equals("")) {
				result = null;
			}
			return result;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return null;
		}
	};

	public static String describe(Locale described, CLDRLocale... describing) {
		String[][] descriptions = new String[describing.length][];
		for (int i=0; i < describing.length; ++i) {
			descriptions[i] = describing[i].getDescriptions(described);
		}
		return makeDescription(describing[0].locale, descriptions);
	}
	
	private static String makeDescription(Locale locale, String[]... langAndTerritory) {
		String[] finalInput = new String[2];
		for (int i=langAndTerritory.length-1; i >= 0; --i) {
			if (finalInput[0] == null) {
				finalInput[0] =  langAndTerritory[i][0];
			}
			if (finalInput[1] == null) {
				finalInput[1] =  langAndTerritory[i][1];
			}
		}
		if (finalInput[0] != null && finalInput[0].length() > 1) {
			finalInput[0] = finalInput[0].substring(0, 1).toUpperCase(locale) + finalInput[0].substring(1);
		}
		if (finalInput[1] != null) {
			return finalInput[0] + " ("+finalInput[1]+")";
		} else {
			return finalInput[0];
		}
	}

	public static Properties getProperties(Locale target) {
		CLDRLocale[] both = CLDRLocale.load(target);
		CLDRLocale authoritative = both[target.getCountry().equals("") ? LANGUAGE_ONLY : LANGUAGE_AND_TERRITORY];
		String supported = both[LANGUAGE_AND_TERRITORY].locale.toString().replace('_', '-');
		Properties props = new SortedProperties(new LocalePropertiesKeyComparator(supported));
		props.put(KEY_LOCALE, both[0].locale.toString());
		props.put(KEY_DISPLAY_NAME, describe(authoritative.locale, ENGLISH));
		props.put(KEY_LOCALES_SUPPORTED, supported);
		props.put(KEY_DISPLAY_NAME_LOC_PREFIX+supported, describe(authoritative.locale, both));
		props.put(KEY_POSIX_ID,"en_US.UTF-8");
		return props;
	}
	
	public static class LocalePropertiesKeyComparator implements Comparator<String> {
		private String[] keys = new String[] {
				KEY_LOCALE, KEY_DISPLAY_NAME, KEY_LOCALES_SUPPORTED, null, KEY_POSIX_ID
		};
		
		public LocalePropertiesKeyComparator(String supported) {
			for (int i=0; i < keys.length; ++i) {
				if (keys[i] == null) {
					keys[i] = KEY_DISPLAY_NAME_LOC_PREFIX + supported;
					break;
				}
			}
		}
		
		@Override
		public int compare(String o1, String o2) {
			int i1 = getArrayPosition(o1);
			int i2 = getArrayPosition(o2);
			return Integer.valueOf(i1).compareTo(Integer.valueOf(i2));
		}

		private int getArrayPosition(String o1) {
			for (int i=0; i < keys.length; ++i) {
				if (keys[i].equals(o1)) {
					return i;
				}
			}
			return -1;
		}
		
	}
}
