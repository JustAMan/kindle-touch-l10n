package ixtab.ktlocale.cldr;

import java.io.InputStream;
import java.net.URL;

public class CLDRLoader {
	public static URL getCLDRFileAsURL(String locale) {
		return CLDRLoader.class.getResource(locale+".xml");
	}
	
	public static InputStream getCLDRFileAsStream(String locale) {
		return CLDRLoader.class.getResourceAsStream(locale+".xml");
	}
}
