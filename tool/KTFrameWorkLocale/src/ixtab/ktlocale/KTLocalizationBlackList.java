package ixtab.ktlocale;


/**
 * A very crude way of blacklisting various patterns from localization.
 * These files have been blacklisted (=explicitly excluded from localizations)
 * because they only contain strings where it doesn't seem to make sense
 * to translate them.
 * 
 * These patterns have been identified manually, i.e. by
 * looking through the "translatable" resources and manually flagging
 * the ones which obviously make no sense to translate.
 * 
 * For now, behavior is hardcoded in this class, but it may well make
 * sense to make the patterns more configurable (e.g. by outsourcing
 * them into a file).
 * 
 * Blacklist patterns match any bundle which starts with the pattern.
 * I.e., the pattern "resource" matches "resource", "resource_de", "resource_ru" etc.
 * As a very special case, if the pattern ends with "$", then an exact match
 * is required. I.e., the pattern "resource$" ONLY matches "resource", but not "resource_de"
 * @author ixtab
 *
 */
public class KTLocalizationBlackList {

	private static String[] BLACKLISTED_CLASSNAMES = new String[] {
					"com.amazon.ebook.util.resources.LcidMappings",
					"com.amazon.ebook.util.resources.MccCodeMappings",
					"com.amazon.kindle.settings.resources.DiagnosticResources",
					"com.amazon.ebook.util.text.stopword.resources.StopWordData",
					"com.amazon.ebook.booklet.reader.resources.DefaultDictionaries",
					"com.amazon.ebook.util.dict.resources.SimpleDictLookupFormatResources",
					"com.amazon.ebook.util.resources.CliticUtilResources",
					"com.amazon.ebook.util.resources.SimpleTextListFormatResources$",
					

	};
	
	public static boolean contains(KTResourceBundle bundle) {
		String canonicalName = bundle.descriptor.toString();
		for (String blacklist: BLACKLISTED_CLASSNAMES) {
			if (blacklist.endsWith("$")) {
				if (canonicalName.equals(blacklist.substring(0, blacklist.length()-1))) {
					System.out.println(canonicalName+" is blacklisted for translation ("+blacklist+").");
					return true;
				}
			}
			if (canonicalName.startsWith(blacklist)) {
				System.out.println(canonicalName+" is blacklisted for translation ("+blacklist+").");
				return true;
			}
		}
		return false;
	}

}
