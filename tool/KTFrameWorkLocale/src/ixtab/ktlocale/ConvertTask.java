package ixtab.ktlocale;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Properties;

public class ConvertTask {

	private final File root;
	public ConvertTask(String source, String target, boolean force) {
		if (!source.equals(target)) {
			throw new IllegalArgumentException("Refusing to run unless source directory equals target directory.");
		}
		if (!force) {
			throw new IllegalArgumentException("Refusing to run unless --force switch is used.");
		}
		root = new File(source);
	}

	public void run() {
		try {
			convert(root);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void convert(File directory) throws IOException {
		for (File file:directory.listFiles()) {
			if (file.isDirectory()) {
				convert(file);
			} else if (file.getName().endsWith(".properties")) {
				convertFile(file);
			}
		}
	}

	private void convertFile(File file) throws IOException {
		// reading from stream uses ISO-8859-1, while writing
		// to Writer uses UTF-8
		Properties props = new Properties();
		FileInputStream fis = new FileInputStream(file);
		props.load(fis);
		fis.close();
		BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
		Writer ow = new OutputStreamWriter(out, "UTF-8");
		props.store(ow, null);
		ow.close();
		PropertyFileNormalizer.normalizeFile(file);
	}

}
