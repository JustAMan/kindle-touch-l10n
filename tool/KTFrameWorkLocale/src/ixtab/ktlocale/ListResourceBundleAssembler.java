package ixtab.ktlocale;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.util.Locale;
import java.util.zip.GZIPOutputStream;

import serp.bytecode.BCClass;
import serp.bytecode.Project;
import serp.bytecode.lowlevel.Entry;
import serp.bytecode.lowlevel.UTF8Entry;
import contrib.Base64Standalone;

public class ListResourceBundleAssembler {
	public static final String DEFAULT_CONTENTS = "H4sIAAAAAAAAAFvzloG1tIhBJDraJyuxLFEvJzEvXc8/KSs1ucRaYv/v4CfZt08xMTBUFDAwMDACFQpjUTfhXMR8gWLNHJg6phIG1rT8opLKEgbmkvJ8ANdAvlljAAAA";
	
	public static BCClass assembleFrom(KTResourceBundle inputBundle) {
		Locale locale = inputBundle.descriptor.locale;
		String packageName = inputBundle.descriptor.packageName;
		String className = inputBundle.descriptor.className;
		BCClass result = new Project().loadClass(ListResourceBundleTemplate.class);
		result.setName(packageName+"."+className + "_" + locale);
		UTF8Entry contents = findContentsEntry(result);
		contents.setValue(stringify(inputBundle.getContents()));
		return result;
	}

	private static UTF8Entry findContentsEntry(BCClass clazz) {
		for (Entry entry: clazz.getPool().getEntries()) {
			if (entry instanceof UTF8Entry) {
				if (DEFAULT_CONTENTS.equals(((UTF8Entry) entry).getValue())) {
					return (UTF8Entry) entry;
				}
			}
		}
		return null;
	}
	
	private static String stringify(Object[][] contents) {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			GZIPOutputStream gos = new GZIPOutputStream(bos);
			ObjectOutputStream oos = new ObjectOutputStream(gos);
			oos.writeObject(contents);
			oos.close();
			String result = Base64Standalone.encodeBytes(bos.toByteArray());
			result = result.replaceAll("[\\r\\n]", "");
			return result;
		} catch (Throwable t) {
			throw new RuntimeException(t);
		}
	}


}
