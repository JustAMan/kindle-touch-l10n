package ixtab.ktlocale;

import ixtab.ktlocale.cldr.CLDRLocale;
import ixtab.ktlocale.kindletool.KindleTool;
import ixtab.ktlocale.util.LocaleUtil;
import ixtab.ktlocale.util.MD5Util;
import ixtab.ktlocale.util.TGZCreator;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.apache.commons.ssl.PKCS8Key;

public class DistributionTask implements Runnable {

	public static final String PATTERN_TYPE = "TYPE";
	public static final String PATTERN_LOCALE = "LOCALE";
	public static final String TYPE_INSTALL = "install";
	public static final String TYPE_UNINSTALL = "uninstall";
	
	private static final List<String> PREINSTALLED_LOCALES = getPreinstalledLocales();
	private static final long BLOCK_SIZE = 64;

	private final Workspace workspace;
	private final List<String> locales;
	private final String target;
	private final boolean force;
	private final MsgFmt msgfmt;
	private final KindleTool kindletool;

	private static List<String> getPreinstalledLocales() {
		/* This is the list of locales that a stock Kindle comes preinstalled with.
		 * No idea why it is this combination... Anyway, these locales will not
		 * be removed when a package is uninstalled.
		 */
		
		List<String> list = new ArrayList<String>();
		list.add("de_DE");
		list.add("en_GB");
		list.add("en_US");
		list.add("es_ES");
		list.add("fr_FR");
		list.add("it_IT");
		list.add("ja_JP");
		list.add("pt_BR");
		list.add("ru_RU");
		list.add("sl_SI");
		list.add("sq_AL");
		list.add("tr_TR");
		list.add("zh_CN");
		list.add("zh_HK");
		list.add("zh_TW");
		return list;
	}

	public DistributionTask(String source, String target, String msgfmt,
			String kindletool, boolean force, String... locales) {
		workspace = new Workspace(new File(source));
		this.locales = evaluateLocaleArray(locales);
		this.target = target;
		this.force = force;
		this.msgfmt = new MsgFmt(msgfmt);
		this.kindletool = new KindleTool(kindletool);
		validateTarget();
	}

	private List<String> evaluateLocaleArray(String[] in) {
		if (in.length == 0)
			throw new IllegalArgumentException(
					"Must provide at least one locale");

		List<String> all = workspace.listAvailableLocales();
		if (in.length == 1 && in[0].equals("ALL")) {
			return all;
		}
		List<String> out = new ArrayList<String>();
		for (String locale : in) {
			if (!all.contains(locale)) {
				throw new IllegalArgumentException("No translation for \""
						+ locale + "\" found. Available translations are: "
						+ all);
			}
			out.add(locale);
		}
		return out;
	}

	private void validateTarget() {
		if (!target.contains(PATTERN_TYPE)) {
			throw new IllegalArgumentException("Invalid target name: " + target
					+ ". Target name must contain substring \"" + PATTERN_TYPE
					+ "\", which will be replaced by \"" + TYPE_INSTALL
					+ "\"/\"" + TYPE_UNINSTALL + "\".");
		}
		if (locales.size() > 1 && !target.contains(PATTERN_LOCALE)) {
			throw new IllegalArgumentException(
					"Invalid target name: "
							+ target
							+ ". For batch distribution creation, target name must contain substring \""
							+ PATTERN_LOCALE
							+ "\", which will be replaced by the locale code.");
		}
	}

	@Override
	public void run() {
		for (String locale : locales) {
			if (locales.size() > 1) {
				System.out.println("Creating distribution: "+locale);
			}
			try {
				createDistribution(locale);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private void createDistribution(String localeString) throws IOException {
		Map<String, File> content = new TreeMap<String, File>();
		Locale locale = LocaleUtil.localeFromString(localeString);
		addBlanketFiles(locale, content);
		addJavaFiles(locale, content);
		addManifest(locale, content);
		createKindlePackages(locale, content);
	}

	private void addBlanketFiles(Locale locale, Map<String, File> content)
			throws IOException {
		String prefix = workspace.blanketDir.getCanonicalPath()
				+ File.separator + locale.toString() + File.separator;
		for (File po : workspace.findBlanketFiles(locale)) {
			String kindleFilename = po.getCanonicalPath();
			if (!kindleFilename.startsWith(prefix)) {
				throw new IllegalStateException();
			}
			kindleFilename = kindleFilename.substring(prefix.length());
			kindleFilename = "/usr/share/locale/" + locale.getLanguage() + "/"
					+ kindleFilename;
			// .po -> .mo
			kindleFilename = kindleFilename.substring(0,
					kindleFilename.length() - 3)
					+ ".mo";
			File mo = makeTemporaryFile(po.getName().replace(".po", ""), ".mo");
			msgfmt.format(po, mo);
			content.put(kindleFilename, mo);
		}
	}

	private void addJavaFiles(Locale locale, Map<String, File> content)
			throws IOException {
		File jar = makeTemporaryFile("locale-" + locale, ".jar");
		new CompressTask(workspace.javaDir.getCanonicalPath(),
				jar.getCanonicalPath(), true, locale.toString()).run();
		content.put("/opt/amazon/ebook/lib/locale-" + locale + ".jar", jar);
	}

	private void addManifest(Locale locale, Map<String, File> content)
			throws IOException {
		String filename = locale.toString() + ".properties";
		File manifest = makeTemporaryFile(locale.toString(), ".properties");
		Properties props = CLDRLocale.getProperties(locale);
		FileOutputStream os = new FileOutputStream(manifest);
		props.store(os, null);
		os.close();
		PropertyFileNormalizer.normalizeFile(manifest);
		content.put("/opt/amazon/ebook/config/locales/" + filename, manifest);
	}

	private File makeTemporaryFile(String prefix, String suffix)
			throws IOException {
		File f;
		f = File.createTempFile(prefix + "-", suffix);
		f.deleteOnExit();
		return f;
	}

	private void createKindlePackages(Locale locale, Map<String, File> content)
			throws IOException {
//		TemporaryDirectory parent = new TemporaryDirectory("locale-"
//				+ locale);
//		
//		File install = new File(parent, TYPE_INSTALL);
//		if (!install.mkdirs()) throw new RuntimeException("Unable to create "+ install.getCanonicalPath());
//		File uninstall = new File(parent, TYPE_UNINSTALL);
//		if (!uninstall.mkdirs()) throw new RuntimeException("Unable to create "+ uninstall.getCanonicalPath());
//		
		Map<String, File> installFiles = createKindleInstallFiles(content, locale);
		Map<String, File> uninstallFiles = createKindleUninstallFiles(locale);
		
		PrivateKey key = loadPrivateKey();
		createKindlePackage(key, TYPE_INSTALL, locale, installFiles);
		createKindlePackage(key, TYPE_UNINSTALL, locale, uninstallFiles);

//		parent.deleteRecursively();
	}

	private Map<String, File> createKindleInstallFiles(Map<String, File> content,
			Locale locale) throws IOException {
		Map<String, File> map = new HashMap<String, File>();
		File archive = makeTemporaryFile("locale-"+locale, ".tgz");
		map.put("locale.tgz", archive);
		TGZCreator.createTGZFile(archive, content);
		File shell = makeTemporaryFile("run", ".ffs");
		map.put("run.ffs", shell);
		createInstallerShellFile(shell, locale);
		return map;
	}

	private void createInstallerShellFile(File file, Locale locale) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("kindletool/install_template.sh")));
		PrintStream out = new PrintStream(file);
		Locale systemLocale = CLDRLocale.load(locale)[CLDRLocale.LANGUAGE_AND_TERRITORY].locale;
		for (String line = in.readLine(); line != null; line = in.readLine()) {
			line = replaceLocalePatterns(line, locale, systemLocale);
			out.println(line);
		}
		in.close();
		out.close();
	}

	private Map<String, File> createKindleUninstallFiles(Locale locale) throws IOException {
		Map<String, File> map = new HashMap<String, File>();
		File shell = makeTemporaryFile("run", ".ffs");
		map.put("run.ffs", shell);
		BufferedReader in = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("kindletool/uninstall_template.sh")));
		PrintStream out = new PrintStream(shell);
		Locale systemLocale = CLDRLocale.load(locale)[CLDRLocale.LANGUAGE_AND_TERRITORY].locale;
		for (String line = in.readLine(); line != null; line = in.readLine()) {
			line = replaceLocalePatterns(line, locale, systemLocale);
			if (line.contains("localedef --delete-from-archive")) {
				if (PREINSTALLED_LOCALES.contains(systemLocale.toString())) {
					continue;
				}
			}
			out.println(line);
		}
		in.close();
		out.close();
		return map;
	}

	private String replaceLocalePatterns(String line, Locale locale, Locale systemLocale) {
		// this is simple and stupid, without regexes
		line = line.replace("<LOC_SHORT>", locale.getLanguage());
		line = line.replace("<LOC_FULL>", locale.toString());
		line = line.replace("<LOC_SYSTEM>", systemLocale.toString());
		return line;
	}

	private PrivateKey loadPrivateKey() throws IOException {
		InputStream is = KindleTool.class.getResourceAsStream("key.pem");
		try {
			return new PKCS8Key(is, null).getPrivateKey();
		} catch (GeneralSecurityException e) {
			throw new IllegalStateException(e);
		}
	}

	private void createKindlePackage(PrivateKey key, String type, Locale locale,
			Map<String, File> sources) throws IOException {
		Map<String, File> finalContents = new HashMap<String, File>();
		StringBuilder manifest = new StringBuilder();
		for (Map.Entry<String, File> entry: sources.entrySet()) {
			addSignatureAndManifest(entry.getKey(), entry.getValue(), key, finalContents, manifest);
		}
		File manifestFile = makeTemporaryFile("update-filelist", ".dat");
		FileWriter fw = new FileWriter(manifestFile);
		fw.append(manifest.toString());
		fw.close();
		addSignatureAndManifest("update-filelist.dat", manifestFile, key, finalContents, null);
		
		File tgz = makeTemporaryFile(type+"-"+locale, ".tgz");
		TGZCreator.createTGZFile(tgz, finalContents);
		invokeKindleTool(locale, type, tgz);
	}
	
	private void addSignatureAndManifest(String filename, File source,
			PrivateKey key, Map<String, File> target,
			StringBuilder manifest) throws IOException {
		if (manifest != null) {
			addManifestEntry(filename, source, manifest);
		}
		String sigName = filename+".sig";
		File sigFile = makeTemporaryFile(filename, ".sig");
		target.put(filename, source);
		target.put(sigName, sigFile);
		signFile(key, source, sigFile);
	}

	private void addManifestEntry(String filename, File source,
			StringBuilder manifest) throws IOException {
		manifest.append(filename.endsWith(".ffs") ? "129":"128");
		manifest.append(" ");
		manifest.append(MD5Util.getMD5Sum(source));
		manifest.append(" ");
		manifest.append(filename);
		manifest.append(" ");
		manifest.append(source.length() / BLOCK_SIZE);
		manifest.append(" ");
		manifest.append(filename);
		manifest.append("\n");
	}

	private void signFile(PrivateKey key, File source, File target) throws IOException {
		try {
			Signature signer = Signature.getInstance("SHA256WithRSA");
			signer.initSign(key);
			BufferedInputStream is = new BufferedInputStream(new FileInputStream(source));
			byte[] buffer = new byte[1024];
			int len;
			while ((len = is.read(buffer)) >= 0) {
			    signer.update(buffer, 0, len);
			};
			is.close();
			byte[] signature = signer.sign();
			BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(target));
			os.write(signature);
			os.close();
		} catch (SignatureException se) {
			throw new IllegalStateException(se);
		} catch (InvalidKeyException ke) {
			throw new IllegalStateException(ke);
		} catch (NoSuchAlgorithmException ne) {
			throw new IllegalStateException(ne);
		}
	}

	private void invokeKindleTool(Locale locale, String type, File tgz) throws IOException {
		String output = target;
		output = output.replace(PATTERN_LOCALE, locale.toString());
		output = output.replace(PATTERN_TYPE, type);
		File bin = new File(output);
		if (bin.exists() && !force) {
			throw new IllegalStateException("File " + bin+ " exists. Refusing to overwrite unless forced.");
		}
		kindletool.pack(tgz, bin);
		System.out.println("Successfully created "+bin.getCanonicalPath());
	}


}
