package ixtab.ktlocale.modification;

import java.util.SortedMap;

public interface MapModificationStrategy {
	void updateResourceBundleMap(SortedMap<String, Object> map);
}
