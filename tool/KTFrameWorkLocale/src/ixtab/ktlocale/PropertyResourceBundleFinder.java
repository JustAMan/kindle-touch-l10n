package ixtab.ktlocale;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import contrib.IterableEnumeration;

public class PropertyResourceBundleFinder implements ResourceBundleFinder {

	private static final String DOT_PROPERTIES = ".properties";
	private static final String FILTER_COM_AMAZON = "com/amazon/";

	@Override
	public void findBundlesInJars(List<ResourceBundle> output,
			List<JarFile> jars) throws IOException {
		for (JarFile jar: jars) {
			findBundlesInJar(output, jar);
		}
	}

	private void findBundlesInJar(List<ResourceBundle> output, JarFile jar) throws IOException {
		for (JarEntry entry: IterableEnumeration.make(jar.entries())) {
			if (isPropertiesFile(entry)) {
				InputStream is = jar.getInputStream(entry);
				ResourceBundle bundle = new PropertyResourceBundle(is);
				String[] packageAndClass = getPackageAndClass(entry.getName().substring(0, entry.getName().length() - DOT_PROPERTIES.length()));
				KTResourceBundle ktBundle = new KTResourceBundle(bundle, packageAndClass[0], packageAndClass[1], null);
				output.add(ktBundle);
			}
		}
	}

	private String[] getPackageAndClass(String name) {
		/* yes, this is yet another method of determining pkg and class name.
		 * Feel free to refactor all the occurences.
		 */
		while (name.startsWith("/")) {
			name = name.substring(1);
		}
		int sep = name.lastIndexOf('/');
		name = name.replace('/', '.');
		String[] pkgName = new String[2];
		pkgName[0] = name.substring(0, sep);
		pkgName[1] = name.substring(sep+1);
		return pkgName;
	}

	private boolean isPropertiesFile(JarEntry entry) {
		String name = entry.getName();
		return (name.endsWith(DOT_PROPERTIES) && name.contains(FILTER_COM_AMAZON));
	}
}
