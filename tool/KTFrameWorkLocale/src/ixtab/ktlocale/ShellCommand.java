package ixtab.ktlocale;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class ShellCommand {
	
	public static class Result {
		public final String out;
		public final String err;
		public final int code;
		
		public Result(String out, String err, int result) {
			super();
			this.out = out;
			this.err = err;
			this.code = result;
		}
	}
	
	public static Result exec(String command) throws Exception {
		StringBuilder out = new StringBuilder();
		StringBuilder err = new StringBuilder();
		int result = exec(command, out, err);
		return new Result(out.toString(), err.toString(), result);
	}
	
	public static int exec(String command, StringBuilder out, StringBuilder err) throws Exception {
		out.delete(0, out.length());
		err.delete(0, out.length());
		
		Process process;
		int result = (process = Runtime.getRuntime().exec(command)).waitFor();
		fillStringBuilder(out, process.getInputStream());
		fillStringBuilder(err, process.getErrorStream());
		return result;
	}

	private static void fillStringBuilder(StringBuilder b, InputStream is)
			throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				is));
		for (String line = reader.readLine(); line != null; line = reader
				.readLine()) {
			b.append(line).append('\n');
		}
	}

}
