package ixtab.ktlocale.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;

public class TGZCreator {
	public static void createTGZFile(File output, Map<String, File> content) throws IOException {
		TarArchiveOutputStream tar = new TarArchiveOutputStream(new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream(output))));
		for (Map.Entry<String, File> file: content.entrySet()) {
			
			String name = file.getKey();
			while (name.startsWith("/")) {
				name = name.substring(1);
			}
			TarArchiveEntry entry = new TarArchiveEntry(file.getValue(),name);
			
			entry.setGroupId(0);
			entry.setGroupName("root");
			entry.setUserId(0);
			entry.setUserName("root");
			
			entry.setMode(name.endsWith(".ffs") ? 0755 : 0644);
			
			tar.putArchiveEntry(entry);
			copyFile(file.getValue(), tar);
			tar.closeArchiveEntry();
		}
		tar.close();
	}

	// not terribly efficient, I know.
	private static void copyFile(File file, OutputStream os) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(file));
		for (int b = is.read(); b != -1; b = is.read()) {
			os.write(b);
		}
		is.close();
	}
}