/* FROM: http://www.rgagnon.com/javadetails/java-0614.html */

package contrib;

import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

@SuppressWarnings("serial")
public class SortedProperties extends Properties {
	private final Comparator<String> comparator;
	
	public SortedProperties(Comparator<String> comparator) {
		this.comparator = comparator;
	}
	
	public SortedProperties() {
		this(null);
	}

	/**
	 * Overrides, called by the store method.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public synchronized Enumeration keys() {
		Enumeration keysEnum = super.keys();
		Vector keyList = new Vector();
		while (keysEnum.hasMoreElements()) {
			keyList.add(keysEnum.nextElement());
		}
		if (comparator == null) {
			Collections.sort(keyList);
		} else {
			Collections.sort(keyList, comparator);
		}
		return keyList.elements();
	}

}