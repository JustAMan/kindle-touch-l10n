��    /      �  C                   1     O     c     x     �     �     �     �  %   �  !        '  &   E  "   l  %   �     �      �     �          +     >     Z     r     �     �     �     �     �     �               .     C     V     n     �     �     �     �     �     �          1     G     f     �  �  �  	   D
     N
     m
     p
  �   �
     !  �   9  6   	     @     W     Z  w   f     �     �     �  .   �     #     &     6     :     M     P     \     `     r     ~     �     �     �     �     �     �  	   �     �     �     �     �     �  �   �     �     �     �  �   �     Y     \     k     /       -         	      +      
   ,          .   %                        #                 *   )                 &                                  (                     "                              !   $            '               langpicker.cancel.label langpicker.confirm.message %s langpicker.ok.label ota.update.errorcode ota.update.failed ota.update.header ota.update.initial ota.update.success shutdown.cust_service.header shutdown.cust_service.header.fontsize shutdown.cust_service.header.rect shutdown.cust_service.message shutdown.cust_service.message.fontsize shutdown.cust_service.message.rect shutdown.cust_service.message.spacing shutdown.reboot.message shutdown.reboot.message.fontsize shutdown.reboot.message.rect shutdown.reboot.message.spacing test.draw.defaults test.draw.defaults.fontsize test.draw.defaults.rect test.draw.defaults.spacing test.no.fontsize test.no.fontsize.rect test.no.fontsize.spacing test.no.rect test.no.rect.fontsize test.no.rect.spacing test.no.spacing test.no.spacing.fontsize test.no.spacing.rect test.screen.header test.screen.header.rect usb.screen.charging.y_offset usb.screen.header usb.screen.header.fontsize usb.screen.header.rect usb.screen.message usb.screen.message.fontsize usb.screen.message.rect usb.screen.message.spacing usb.screen.message_nc usb.screen.message_nc.fontsize usb.screen.message_nc.rect usb.screen.message_nc.spacing Project-Id-Version: Kindle Touch locale
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-03-23 15:43-0700
PO-Revision-Date: 2012-01-02 02:05+0000
Last-Translator: ixtab <ixtab@fs-xtc.net>
Language-Team: German (http://www.transifex.net/projects/p/kindle-touch-locale/team/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1)
 Abbrechen %s als Gerätesprache wählen? OK Aktualisierungs-Fehler: %s Die Aktualisierung ist fehlgeschlagen.

Besuchen Sie www.kindle.com/support, um Hilfe zu erhalten.
Zum Fortfahren drücken Sie die  Schaltfläche. Software-Aktualisierung Ihre Kindle-Software wird aktualisiert. Dies kann einige Minuten dauern.

Schalten Sie Ihren Kindle während der Aktualisierung NICHT aus.

Ihr Kindle wird neustarten, nachdem die Aktualisierung beendet ist. Aktualisierung erfolgreich.

Ihr Kindle startet neu... Reparatur erforderlich 26 0,34,600,28 Ihr Kindle muss repariert werden.

Bitte wenden Sie sich an den Kindle-Kundendienst auf 
http://www.kindle.com/support. 26 68,384,464,416 2.5 Bitte warten Sie, während Ihr Kindle startet. 26 124,640,352,160 2.5 Test Draw Defaults 26 0,34,600,28 2.5 No fontsize entry 0,34,600,28 2.5 No rect entry 26 2.5 No fontsize entry 26 0,34,600,28 Test Mode 10,42,580,32 20 USB-Laufwerk-Modus 26 0,34,600,28 Wenn Sie Ihren Kindle zum Lesen oder Einkaufen verwenden wollen, während Sie ihn über das USB-Kabel laden, bitte lassen Sie das Kabel eingesteckt, aber werfen Sie das Laufwerk am Computer aus. 26 68,384,464,200 2.5 Wenn Sie Ihren Kindle verwenden wollen, werfen Sie bitte das Kindle-Laufwerk am Computer aus.

Im Augenblick wird Ihr Kindle nicht geladen. 26 68,384,464,200 2.5 