# Translators:
# Translators:
# Translators:
# Martin Novak <martin.novak@seznam.cz>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: Kindle Touch locale\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2011-03-23 15:43-0700\n"
"PO-Revision-Date: 2012-01-02 14:35+0000\n"
"Last-Translator: Martin Novak <martin.novak@seznam.cz>\n"
"Language-Team: Czech (http://www.transifex.net/projects/p/kindle-touch-locale/team/cs/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: cs\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2\n"

msgid "langpicker.cancel.label"
msgstr "Storno"

msgid "langpicker.confirm.message %s"
msgstr "Vybrat %s jako jazyk pro Kindle?"

msgid "langpicker.ok.label"
msgstr "OK"

msgid "ota.update.errorcode"
msgstr "Chyba aktualizace: %s"

msgid "ota.update.failed"
msgstr ""
"Aktualizace neproběhla správně.\n"
"\n"
"Pokud potřebujete pomoc navštivte www.kindle.com/support.\n"
"\n"
"To resume using your Kindle, tap the  button."

msgid "ota.update.header"
msgstr "Aktualizace softwaru."

msgid "ota.update.initial"
msgstr ""
"Your Kindle software is updating. This can take a few minutes.\n"
"\n"
"DO NOT turn your Kindle off during the update process.\n"
"\n"
"Your Kindle will automatically restart as soon as the update is complete."

msgid "ota.update.success"
msgstr ""
"Aktualizace proběhla úspěšně.\n"
"\n"
"Váš Kindle se právě restartuje..."

msgid "shutdown.cust_service.header"
msgstr "Repair Needed"

msgid "shutdown.cust_service.header.fontsize"
msgstr "26"

msgid "shutdown.cust_service.header.rect"
msgstr "0,34,600,28"

msgid "shutdown.cust_service.message"
msgstr ""
"Your Kindle Needs Repair.\n"
"\n"
"Please contact Kindle Customer Service at \n"
"http://www.kindle.com/support."

msgid "shutdown.cust_service.message.fontsize"
msgstr "26"

msgid "shutdown.cust_service.message.rect"
msgstr "68,384,464,416"

msgid "shutdown.cust_service.message.spacing"
msgstr "2.5"

msgid "shutdown.reboot.message"
msgstr "Please wait a moment while your Kindle starts up."

msgid "shutdown.reboot.message.fontsize"
msgstr "26"

msgid "shutdown.reboot.message.rect"
msgstr "124,640,352,160"

msgid "shutdown.reboot.message.spacing"
msgstr "2.5"

msgid "test.draw.defaults"
msgstr "Test Draw Defaults"

msgid "test.draw.defaults.fontsize"
msgstr "26"

msgid "test.draw.defaults.rect"
msgstr "0,34,600,28"

msgid "test.draw.defaults.spacing"
msgstr "2.5"

msgid "test.no.fontsize"
msgstr "No fontsize entry"

msgid "test.no.fontsize.rect"
msgstr "0,34,600,28"

msgid "test.no.fontsize.spacing"
msgstr "2.5"

msgid "test.no.rect"
msgstr "No rect entry"

msgid "test.no.rect.fontsize"
msgstr "26"

msgid "test.no.rect.spacing"
msgstr "2.5"

msgid "test.no.spacing"
msgstr "No fontsize entry"

msgid "test.no.spacing.fontsize"
msgstr "26"

msgid "test.no.spacing.rect"
msgstr "0,34,600,28"

msgid "test.screen.header"
msgstr "Testovací režim"

msgid "test.screen.header.rect"
msgstr "10,42,580,32"

msgid "usb.screen.charging.y_offset"
msgstr "20"

msgid "usb.screen.header"
msgstr "USB Drive Mode"

msgid "usb.screen.header.fontsize"
msgstr "26"

msgid "usb.screen.header.rect"
msgstr "0,34,600,28"

msgid "usb.screen.message"
msgstr ""
"If you want to read or shop on your Kindle while continuing to charge over "
"USB, please keep the USB cable attached, but eject your Kindle from your "
"computer."

msgid "usb.screen.message.fontsize"
msgstr "26"

msgid "usb.screen.message.rect"
msgstr "68,384,464,200"

msgid "usb.screen.message.spacing"
msgstr "2.5"

msgid "usb.screen.message_nc"
msgstr ""
"If you wish to use your Kindle, please eject your Kindle from your computer.\n"
"\n"
"Currently your Kindle is not charging."

msgid "usb.screen.message_nc.fontsize"
msgstr "26"

msgid "usb.screen.message_nc.rect"
msgstr "68,384,464,200"

msgid "usb.screen.message_nc.spacing"
msgstr "2.5"


